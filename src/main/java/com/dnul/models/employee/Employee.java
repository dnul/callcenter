package com.dnul.models.employee;

import com.dnul.models.Call;
import com.dnul.models.employee.impl.EmployeeIsBusyException;
import com.dnul.models.employee.impl.EmployeeRank;

public interface Employee extends Runnable {

	public boolean isAvailable();

	public void assignCall(Call aCall) throws EmployeeIsBusyException;
	
	public EmployeeRank getRank();

}
