package com.dnul.models.employee;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import com.dnul.models.Call;
import com.dnul.models.employee.impl.EmployeeIsBusyException;
import com.dnul.models.employee.impl.EmployeeRank;
import com.dnul.models.employee.impl.WorkStatus;

/**
 * @author dnul
 * 
 * Core implementation of the employee, see design notes in the Readme
 */
public abstract class AbstractEmployee implements Employee,Runnable{
	
	private static Logger logger = Logger.getRootLogger();

	/**
	 *  a simple id for employees
	 */
	private String employeeId;

	/**
	 * rank of the employee used for assigning calls
	 */
	private EmployeeRank rank;

	/**
	 * wheter the current employee is busy or available
	 */
	private WorkStatus status;

	/**
	 * Modeled call as a one-element queue, this is conceptually not ok ( as it is not a queue)
	 * but its beneficial for implementation purposes ( gives easy communication between threads + blocking capacity)  
	 */
	private BlockingQueue<Call> currentCall = new ArrayBlockingQueue<Call>(1);

	public AbstractEmployee(String id, EmployeeRank rank) {
		this.rank = rank;
		this.employeeId = id;
		this.status=WorkStatus.AVAILABLE;
	}
	
	/* 
	 * 
	 * (non-Javadoc)
	 * @see com.dnul.models.employee.Employee#assignCall(com.dnul.models.Call)
	 * 
	 * set status to busy and adds call to the queue
	 */
	public void assignCall(Call aCall) throws EmployeeIsBusyException{
		if(!this.isAvailable()){
			throw new EmployeeIsBusyException();
		}
		
		this.setStatus(WorkStatus.ONCALL);
		this.currentCall.add(aCall);
	}

	/**
	 * This is the actual simulation of a call ( just waits *duration* seconds)  
	 * @param aCall
	 */
	public void pickupCall(Call aCall) {
		// simulate call
		int duration = aCall.getDuration();

		try {
			logger.info("duration for call " + aCall.getOrigin() +" is: " + duration);
			TimeUnit.SECONDS.sleep(duration);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 * 
	 * An employee basically blocks until a call arrives
	 */
	public void run() {
		
		this.setStatus(WorkStatus.AVAILABLE);
		
		while (true) {
			
			try {
				// blocks
				logger.info(this + " waiting for call");
				Call aCall = currentCall.take();
				logger.info( this + " received new call: " + aCall.getOrigin());
				this.setStatus(WorkStatus.ONCALL);
				
				this.pickupCall(aCall);
				
				this.setStatus(WorkStatus.AVAILABLE);
				logger.info( this+ " end call: " + aCall.getOrigin());
			} catch (InterruptedException e) {
				throw new RuntimeException("shutting down callcenter");
			}

		}

	}
	
	//this two methods are synchronized so that there are no race conditions 
	public synchronized boolean isAvailable() {
		return this.status.equals(WorkStatus.AVAILABLE);
	}
	
	public synchronized void setStatus(WorkStatus status) {
		this.status = status;
	}

	public WorkStatus getStatus() {
		return status;
	}

	

	public EmployeeRank getRank() {
		return rank;
	}

	public void setRank(EmployeeRank rank) {
		this.rank = rank;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	
	@Override
	public String toString() {
		return "[employeeId=" + employeeId + ", rank=" + rank + "]";
	}

}
