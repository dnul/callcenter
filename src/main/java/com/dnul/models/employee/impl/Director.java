package com.dnul.models.employee.impl;

import com.dnul.models.employee.AbstractEmployee;

public class Director extends AbstractEmployee {

	public Director(String id) {
		super(id, EmployeeRank.DIRECTOR);
	}

}
