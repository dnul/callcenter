package com.dnul.models.employee.impl;

public class EmployeeIsBusyException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8286231065404848430L;

	public EmployeeIsBusyException() {
		super();
	}

}
