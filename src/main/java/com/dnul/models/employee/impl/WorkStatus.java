package com.dnul.models.employee.impl;

public enum WorkStatus {
	ONCALL,
	AVAILABLE,
	OUT
}
