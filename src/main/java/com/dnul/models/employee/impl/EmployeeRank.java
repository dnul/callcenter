package com.dnul.models.employee.impl;

public enum EmployeeRank {
	OPERATOR(1), SUPERVISOR(2), DIRECTOR(3);

	private Integer rank;

	EmployeeRank(int rank) {
		this.setRank(rank);
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

}
