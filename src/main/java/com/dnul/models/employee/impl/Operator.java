package com.dnul.models.employee.impl;

import com.dnul.models.employee.AbstractEmployee;

public class Operator extends AbstractEmployee {

	public Operator(String id) {
		super(id, EmployeeRank.OPERATOR);
	}

}
