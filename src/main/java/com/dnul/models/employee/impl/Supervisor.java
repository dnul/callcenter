package com.dnul.models.employee.impl;

import com.dnul.models.employee.AbstractEmployee;

/**
 * @author dnul
 * 
 */
public class Supervisor extends AbstractEmployee {

	
	//Employee hierarchy could be enhanced implementing references to *director* and *operator* instances, but 
	//was not necessary for the given task
	
	
	public Supervisor(String id) {
		super(id, EmployeeRank.SUPERVISOR);
	}

}
