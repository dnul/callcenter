package com.dnul.models;

import java.util.List;

import com.dnul.models.employee.Employee;
import com.dnul.service.CallDispatcher;
import com.dnul.service.impl.AllLinesAreBusyException;

/**
 * @author dnul
 * models a callcenter consisting of employees and a call dispatcher
 *
 */
public class CallCenter {
	
	//this could be injected
	private CallDispatcher dispatcher;
	
	private List<Employee> employees;
	
	
	public CallCenter(){
	}
	
	public CallCenter(CallDispatcher dispatcher,List<Employee> employees) {
		this.dispatcher=dispatcher;
		this.employees=employees;
	}
	

	public CallDispatcher getDispatcher() {
		return dispatcher;
	}

	public void setDispatcher(CallDispatcher dispatcher) {
		this.dispatcher = dispatcher;
	}
	
	/**
	 * On any day, callcenter starts and employee login / registers to the call dispatcher
	 */
	public void start(){
		dispatcher.registerEmployees(employees);
		dispatcher.start();
	}
	
	
	/**
	 * Receives a phone call from the outside world
	 * @param aCall
	 * @throws AllLinesAreBusyException
	 */
	public void receiveCall(Call aCall) throws AllLinesAreBusyException{
		dispatcher.DispatchCall(aCall);
	}
	

}
