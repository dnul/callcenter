package com.dnul.models;

import java.util.Random;

public class Call {

	private static int MIN_CALL_DURATION = 5;

	private static int MAX_CALL_DURATION = 10;

	private int duration;

	private String origin;

	public Call(String origin) {
		this.setOrigin(origin);
		// generate random call duration
		Random random = new Random();
		double nextDouble = random.nextDouble();
		Double duration = MIN_CALL_DURATION + nextDouble * (MAX_CALL_DURATION - MIN_CALL_DURATION);
		this.setDuration(duration.intValue());

	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	@Override
	public String toString() {
		return "Call [origin=" + origin + "]";
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

}
