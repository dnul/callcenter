package com.dnul;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.dnul.models.Call;
import com.dnul.models.CallCenter;
import com.dnul.models.employee.Employee;
import com.dnul.models.employee.impl.Director;
import com.dnul.models.employee.impl.Operator;
import com.dnul.models.employee.impl.Supervisor;
import com.dnul.service.impl.AllLinesAreBusyException;
import com.dnul.service.impl.QueueRankCallDispatcher;
import com.dnul.service.impl.RankCallDispatcher;


/**
 * Callcenter App , main class
 *
 */
public class App {
	
    private static Logger log = Logger.getRootLogger(); 

    //general parameters
	private static int NUMBER_OF_OPERATORS = 10;
	private static int NUMBER_OF_SUPERVISORS = 1;
	private static int NUMBER_OF_DIRECTORS = 1;

	public static void main(String[] args) {

		//create employeees
		List<Employee> employees = new ArrayList<Employee>();
		for (int i = 0; i < NUMBER_OF_OPERATORS; i++) {
			employees.add(new Operator("operator-" + i));
		}

		for (int i = 0; i < NUMBER_OF_SUPERVISORS; i++) {
			employees.add(new Supervisor("operator-" + i));
		}

		for (int i = 0; i < NUMBER_OF_DIRECTORS; i++) {
			employees.add(new Director("operator-" + i));
		}

		//create callcenter with dispatcher
		
		//simple dispatcher commented out 
//		CallCenter callCenter = new CallCenter(new RankCallDispatcher(), employees);
		CallCenter callCenter = new CallCenter(new QueueRankCallDispatcher(), employees);

		callCenter.start();

		//simulate arriving calls every second
		while (true) {
			try {
				TimeUnit.SECONDS.sleep(1);
				Call call = new Call("call-" + System.currentTimeMillis());
				log.info("creating new call " + call.toString());
				callCenter.receiveCall(call);

			} catch (AllLinesAreBusyException e) {
				log.error("call failed, all lines busy");
			} catch(InterruptedException e){
				throw new RuntimeException();
			}
		}

	}
}
