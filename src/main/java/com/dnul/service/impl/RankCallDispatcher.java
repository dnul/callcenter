package com.dnul.service.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.dnul.models.Call;
import com.dnul.models.employee.Employee;
import com.dnul.models.employee.impl.EmployeeIsBusyException;
import com.dnul.service.CallDispatcher;

/**
 * @author dnul
 * 
 * Basic implementation of the call dispatcher, it receives call while there are 
 * available employeees, if not it throw an exception discarding the call. 
 *
 */
public class RankCallDispatcher implements CallDispatcher {
	
	private static Logger logger = Logger.getRootLogger();

	private List<Employee> employees;

	/* (non-Javadoc)
	 * @see com.dnul.service.CallDispatcher#DispatchCall(com.dnul.models.Call)
	 * 
	 * when a call arrives looks for available employees and assigns the call to
	 * the first one ( they are ordered by ascending rank, so no director should take a call while an operator is available)
	 */
	public void DispatchCall(Call aCall) throws AllLinesAreBusyException {
		boolean foundAvailable = false;
		
		List<Employee> available = this.getAvailableEmployees();
		
		Iterator<Employee> it = available.iterator();
		while(it.hasNext() && !foundAvailable){
			Employee emp = it.next();
			try {
				emp.assignCall(aCall);
				foundAvailable=true;
			} catch (EmployeeIsBusyException e1) {
				logger.error("error assigning call" + aCall + "to employee:" + emp  + " looking for another...");
			}
		}
		
		
		if(!foundAvailable){
			logger.error(" no employees available, ignoring call");
			throw new AllLinesAreBusyException();
		}
	}

	/**
	 * filters out employees that are not busy in calls
	 * @return
	 */
	public List<Employee> getAvailableEmployees() {
		return getEmployees().stream().filter( emp -> emp.isAvailable()).collect(Collectors.toList());
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	/* (non-Javadoc)
	 * @see com.dnul.service.CallDispatcher#start()
	 * 
	 * Here I made a core modelling decision, i've decided each employee to be the
	 * thread that handles each of the calls the dispatcher receives. I found this way 
	 * more natural than having a fixedThreadpool in the dispatcher because there is a 
	 * 1 to 1 relation between the concurrent capacity of the dispatcher and the number of employees available.
	 * 
	 * When the dispatcher starts, a thread for each of the employees is created, sharing employee instance variables
	 * and communicating through a queue.
	 * 
	 */
	public void start() {
		for (Employee e : employees) {
			Thread t = new Thread(e);
			t.start();
		}

	}

	public void registerEmployees(List<Employee> employees) {
		this.employees = employees;
		
		//this ensures calls are assigned respecting employee's rank
		Collections.sort(this.employees, new Comparator<Employee>() {

			public int compare(Employee o1, Employee o2) {
				return o1.getRank().getRank() <= o2.getRank().getRank() ? -1 : 1;
			}
		});

	}

}
