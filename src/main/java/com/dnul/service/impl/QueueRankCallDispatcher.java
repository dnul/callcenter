package com.dnul.service.impl;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.dnul.models.Call;
import com.dnul.service.CallDispatcher;

/**
 * @author dnul Now all calls get into a queue, a
 */
public class QueueRankCallDispatcher extends RankCallDispatcher implements CallDispatcher, Runnable {

	private static final int MAX_QUEUE_LENGTH = 100;

	private static Logger logger = Logger.getRootLogger();

	/**
	 * Calls are put into a queue, queue size should be large enough to handle calls workload
	 */
	BlockingQueue<Call> callQueue = new ArrayBlockingQueue<Call>(MAX_QUEUE_LENGTH);

	public QueueRankCallDispatcher() {
		super();
	}
	
	public void start(){
		super.start();
		//Starts the call dispatcher polling
		new Thread(this).start();
	}

	public void DispatchCall(Call aCall) {
		try{
			callQueue.add(aCall);
		}catch (IllegalArgumentException e) {
			throw new RuntimeException("callqueue is full, callcenter has fallen");
		}
	}

	public void run() {
		try {
			while (true) {
				Call aCall;
				//blocks until a call arrives
				aCall = callQueue.take();

				//poll employees until someone is free
				while (this.getAvailableEmployees().isEmpty()) {
					// wait some time until someone is free
					TimeUnit.SECONDS.sleep(5l);
				}
				
				// now,someone is free
				logger.info(this.getAvailableEmployees().size() + " employees free, " + callQueue.size() + " calls in line");
				try {
					super.DispatchCall(aCall);
				} catch (AllLinesAreBusyException e) {
					logger.error("something went wrong, lines still busy , keep waiting");
					// return call to queue
					callQueue.add(aCall);
				}
			}
		} catch (InterruptedException e) {
			logger.error("interrupted the callDispatcher, aborting..");
		}
	}
}
