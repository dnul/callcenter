package com.dnul.service;

import java.util.List;

import com.dnul.models.Call;
import com.dnul.models.employee.Employee;
import com.dnul.service.impl.AllLinesAreBusyException;

/**
 * @author dnul
 * This will be the 'dispatcher' class mentioned in the excercise document
 */
public interface CallDispatcher {
	
	public void DispatchCall(Call aCall) throws AllLinesAreBusyException;
	
	public void registerEmployees(List<Employee> employees);

	public void start();

}
