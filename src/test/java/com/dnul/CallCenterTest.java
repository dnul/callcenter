package com.dnul;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.dnul.models.Call;
import com.dnul.models.employee.Employee;
import com.dnul.models.employee.impl.Operator;
import com.dnul.service.CallDispatcher;
import com.dnul.service.impl.AllLinesAreBusyException;
import com.dnul.service.impl.QueueRankCallDispatcher;
import com.dnul.service.impl.RankCallDispatcher;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
@RunWith(JUnit4.class)
public class CallCenterTest extends TestCase {

	private static final int LONG_CALL_DURATION = 1000000;
	CallDispatcher cd = null;
	QueueRankCallDispatcher qcd;
	
	@Before
	public void tearUp(){
		this.cd = new RankCallDispatcher();
		this.qcd= new QueueRankCallDispatcher();
	}

	@Test
	public void testCallCenterWith10ConcurrentCalls() throws AllLinesAreBusyException, InterruptedException{
		List<Employee> operators = new ArrayList<Employee>();
		for (int i = 0; i < 10; i++) {
			Operator operator = new Operator("operator-"+i);
			operators.add(operator);
		}
		
		cd.registerEmployees(operators);
		cd.start();
		
		for (int i = 0; i < 10; i++) {
			Call call = new Call("origin-1");
			call.setDuration(LONG_CALL_DURATION);
			cd.DispatchCall(call);
		}
		
		//wait for threads to kick-in
		Thread.sleep(1000l);
		
		List<Employee> availableEmployees = ((RankCallDispatcher)cd).getAvailableEmployees();
		assertTrue(availableEmployees.isEmpty());
		
	}
	
	@Test
	public void testQueueCallCenterWith10ConcurrentCalls() throws AllLinesAreBusyException, InterruptedException{
		List<Employee> operators = new ArrayList<Employee>();
		for (int i = 0; i < 10; i++) {
			Operator operator = new Operator("operator-"+i);
			operators.add(operator);
		}
		
		qcd.registerEmployees(operators);
		qcd.start();
		
		for (int i = 0; i < 10; i++) {
			Call call = new Call("origin-1");
			call.setDuration(LONG_CALL_DURATION);
			qcd.DispatchCall(call);
		}
		
		//wait for threads to kick-in
		Thread.sleep(1000l);
		
		List<Employee> availableEmployees = ((QueueRankCallDispatcher)qcd).getAvailableEmployees();
		System.out.println(availableEmployees);
		assertTrue(availableEmployees.isEmpty());
		
	}
	

}
