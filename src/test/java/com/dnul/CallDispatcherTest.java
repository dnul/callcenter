package com.dnul;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.dnul.models.Call;
import com.dnul.models.employee.Employee;
import com.dnul.models.employee.impl.Director;
import com.dnul.models.employee.impl.EmployeeIsBusyException;
import com.dnul.models.employee.impl.Operator;
import com.dnul.models.employee.impl.Supervisor;
import com.dnul.models.employee.impl.WorkStatus;
import com.dnul.service.CallDispatcher;
import com.dnul.service.impl.AllLinesAreBusyException;
import com.dnul.service.impl.RankCallDispatcher;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
@RunWith(JUnit4.class)
public class CallDispatcherTest extends TestCase {

	private static final int LONG_CALL_DURATION = 1000000;
	CallDispatcher cd = new RankCallDispatcher();

	@Test
	public void testEmployeeIsUnavailableAfterAssigningCall() throws EmployeeIsBusyException, InterruptedException {
		Operator operator = new Operator("a");
		operator.setStatus(WorkStatus.AVAILABLE);

		Thread thread = new Thread(operator);
		thread.start();
		Call call = new Call("origin-1");

		call.setDuration(LONG_CALL_DURATION);
		operator.assignCall(call);
		assertFalse(operator.isAvailable());
	}

	@Test(expected = EmployeeIsBusyException.class)
	public void testOperatorCantAcceptTwoCalls() throws EmployeeIsBusyException {
		Operator operator = new Operator("a");

		Thread thread = new Thread(operator);
		thread.start();
		Call call = new Call("origin-1");

		call.setDuration(LONG_CALL_DURATION);
		operator.assignCall(call);
		operator.assignCall(call);
	}

	@Test
	public void testCanHandleSimultaneousCall() throws AllLinesAreBusyException {
		Operator operator = new Operator("a");
		Operator anotherOperator = new Operator("b");

		List<Employee> employees = Arrays.asList(operator, anotherOperator);

		cd.registerEmployees(employees);
		cd.start();

		Call call = new Call("origin-1");
		call.setDuration(LONG_CALL_DURATION);

		Call anotherCall = new Call("origin-1");
		anotherCall.setDuration(LONG_CALL_DURATION);

		cd.DispatchCall(call);
		cd.DispatchCall(anotherCall);

	}

	@Test(expected = AllLinesAreBusyException.class)
	public void testLinesAreBusy() throws AllLinesAreBusyException {
		Operator operator = new Operator("a");

		List<Employee> employees = Arrays.asList(operator);

		cd.registerEmployees(employees);
		cd.start();

		Call call = new Call("origin-1");
		call.setDuration(LONG_CALL_DURATION);

		cd.DispatchCall(call);
		cd.DispatchCall(call);
	}

	@Test
	@Before
	public void testOneOperatorRemainsAvailable() throws AllLinesAreBusyException {
		Operator operator = new Operator("a");
		Operator anotherOperator = new Operator("b");

		List<Employee> employees = Arrays.asList(operator, anotherOperator);

		cd.registerEmployees(employees);
		cd.start();

		Call call = new Call("origin-1");
		call.setDuration(LONG_CALL_DURATION);

		cd.DispatchCall(call);

		List<Employee> availableEmployees = ((RankCallDispatcher) cd).getAvailableEmployees();
		assertEquals(availableEmployees.size(), 1);
	}

	@Test
	public void testSupervisorRemainsAvailable() throws AllLinesAreBusyException {
		Operator operator = new Operator("a");
		Supervisor supervisor = new Supervisor("b");

		List<Employee> employees = Arrays.asList(operator, supervisor);

		cd.registerEmployees(employees);
		cd.start();

		Call call = new Call("origin-1");
		call.setDuration(LONG_CALL_DURATION);

		cd.DispatchCall(call);

		List<Employee> availableEmployees = ((RankCallDispatcher) cd).getAvailableEmployees();
		assertEquals(availableEmployees.size(), 1);
		assertTrue(availableEmployees.contains(supervisor));
	}

	@Test
	public void testDirectorRemainsAvailable() throws AllLinesAreBusyException {
		Operator operator = new Operator("a");
		Supervisor supervisor = new Supervisor("b");
		Director director = new Director("c");

		List<Employee> employees = Arrays.asList(operator, supervisor, director);

		cd.registerEmployees(employees);
		cd.start();

		Call call = new Call("origin-1");
		call.setDuration(LONG_CALL_DURATION);

		cd.DispatchCall(call);
		cd.DispatchCall(call);

		List<Employee> availableEmployees = ((RankCallDispatcher) cd).getAvailableEmployees();
		assertEquals(availableEmployees.size(), 1);
		assertTrue(availableEmployees.contains(director));
	}

}
