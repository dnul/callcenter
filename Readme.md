
***CallCenter excercise app*** 

**deliverables**

The main class executes a sample call center containing 10 operators, 1 supervisor and 1 director which receives a call every second. 

Tests include another sample call center which receives 10 calls simultaneously. 

**to run:** 

    mvn compile package exec:java

to run tests:

    mvn test





**Project Structure**

Main class is located under **src/main/java/com/dnul/App.java**

Model classes are under **src/main/java/com/dnul/models**

Call dispatcher and implementations are **src/main/java/com.dnul/service/**

tests under **src/test/java/CallCenterTest.java** include  general testing of the Callcenter
**and src/test/java/CallDispatcherTest.java** includes more detailed unit testing of the dispatchers.



**Design notes**

- CallCenter does not support adding/removing employees once started.
- Hierarchy between supervisor, operator, director was not modeled as it is not relevant for the problem ( just used their ranks)
- Two *dispatcher* classes are implemented: *RankCallDispatcher*,*QueueRankCallDispatcher*
- *QueueRankCallDispatcher* puts calls on a queue when there are no available employees
- *RankCallDispatcher* throws an exception when there are no available employees and discards the call.
- both CallDispatchers assign calls according to the rank of the employees by ordering the employees at the beginning. 
- Employees base class is AbstractEmployee and implements runnable. I've found that treating each employee as 
 a call consumer a good approach to model the call center.  This way, we can handle phone calls concurrently assigning a thread
   per employee. Employees are threads that wait for calls to arrive. Another possibility to explore was using a threadPool in the callDispatcher. 
- In the *queueRankCallDispatcher* some polling is implemented to wait for employees to be available, this can be improved by enhancing communication between threads( i.e letting the dispatcher know when a call ends).
       

**disclosure**

This project was made with limited time in the course of an afternoon + some hours in the evening. There are many ways to enhance it.  
